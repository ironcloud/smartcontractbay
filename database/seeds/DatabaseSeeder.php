<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\User::class)->create(['first_name'=>'Arif', 'last_name'=>'Iqbal','email'=>'arifiqbal@outlook.com','password' => bcrypt('arifiqbal')]);
        factory(App\Contract::class, 50)->create();
    }
}
