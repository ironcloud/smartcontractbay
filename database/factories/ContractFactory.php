<?php

use Faker\Generator as Faker;

$factory->define(App\Contract::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph(5),
        'author' => $faker->name,
        'platform' => $faker->word,
        'language' => $faker->languageCode,
        'comments' => $faker->sentence,
        'thumbnail' => 'public/contracts/' . $faker->image(storage_path('app/public/contracts'), 100, 100, null, false),
        'is_free' => $faker->boolean,
        'rating' => $faker->randomFloat(1,3,5),
        'attachment' => 'public/contracts/' . $faker->image(storage_path('app/public/contracts'), 900, 350, null, false),
        'user_id' => function(){
            return factory(App\User::class)->create();
        }
    ];
});
