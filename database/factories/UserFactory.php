<?php

use Faker\Generator as Faker;


/** @var \Faker\Factory $factory */
$factory->define(App\User::class, function (Faker $faker) {
    $gender = ['male', 'female'];
    $gender_offset = rand(0, 1);

    return [
        'first_name' => $faker->name($gender[$gender_offset]),
        'last_name' => $faker->name($gender[$gender_offset]),
        'public_address' => $faker->address,
        'gender' => $gender[$gender_offset],
        'dp' => 'public/images/profile/' . $faker->image(storage_path('app/public/images/profile'), 150, 150, null, false),
        'banner' => null,
        'city' => $faker->city,
        'country' => $faker->country,
        'about' => $faker->paragraph,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
