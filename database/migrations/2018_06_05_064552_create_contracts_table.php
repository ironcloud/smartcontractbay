<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('author')->nullable();
            $table->string('platform')->nullable();
            $table->string('language')->nullable();
            $table->string('comments')->nullable();
            $table->string('thumbnail')->nullable();
            $table->boolean('is_free')->default(false);
            $table->float('rating')->default(0);
            $table->float('price')->default(0);
            $table->unsignedSmallInteger('download')->default(0);
            $table->string('attachment')->nullable();
            $table->unsignedInteger('user_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
