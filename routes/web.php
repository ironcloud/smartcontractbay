<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/{a?}/{b?}/{c?}/{d?}', function ($a = false, $b = false, $c = false, $d = false) {
    return view('welcome');
});

//Route::view('/{any?}/{any?}/{any?}/{any?}', 'welcome');


Route::get('/about', function () {
    return 'about';
});

Route::get('/skills', function () {
    return ['php', 'laravel', 'codeigniter', 'javascript', 'jquery', 'Vuejs'];
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
