<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::post('signup', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::group(['middleware' => 'jwt.auth'], function(){
    Route::post('auth/logout', 'AuthController@logout');
    Route::get('auth/user', 'AuthController@user');

    Route::post('user/upload-dp', 'UsersController@uploadDp');
    Route::put('profile', 'UsersController@profile');
    Route::get('contracts/my', 'Api\ContractsController@myContract');
    Route::post('contracts/{contract}/download', 'Api\ContractsController@download');
    Route::get('users', 'UsersController@index');
});

Route::middleware('jwt.refresh')->get('/token/refresh', 'AuthController@refresh');


Route::group(['namespace' => 'Api'], function ()
{
    Route::get('contracts', 'ContractsController@index');
    Route::get('contracts/{id}', 'ContractsController@show');

    Route::group(['middleware' => 'jwt.auth'], function ()
    {
        Route::post('contracts', 'ContractsController@store');
        Route::put('contracts/{contract}', 'ContractsController@update');
        Route::post('contracts/{contract}/rate', 'ContractsController@rating');
        Route::post('contract_uploads/{contract_id?}', 'UploadsController@contractUploads');
        Route::post('contract_thumbnail', 'UploadsController@contractThumbnail');
        Route::delete('delete_file', 'UploadsController@destroyFile');
    });
});
