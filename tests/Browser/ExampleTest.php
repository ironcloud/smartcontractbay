<?php

namespace Tests\Browser;

use Tests\Browser\Pages\About;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class HomePageTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testAboutLink()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->click('.links a:first-child')
                ->on(new About)
                ->assertSee('about');
        });
    }

    /**
     * A basic browser test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testDocumentationLink()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->click('.links a:nth-child(2)')
                ->assertSee('laravel');
        });
    }
}
