<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'public_address', 'gender', 'email', 'password', 'dp', 'city', 'country', 'about', 'banner'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    public function addContract($contract)
    {
        $this->contracts()->save($contract);
    }

    public function ratings()
    {
        return $this->morphMany(Rating::class, 'author');
    }

//    public function getDpAttribute(){
//        dd($this->toArray());
//        return public_filepath($this->dp);
//    }
}
