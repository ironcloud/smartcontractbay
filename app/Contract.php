<?php

namespace App;

use Ghanem\Rating\Traits\Ratingable;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use Ratingable;

    protected $fillable = ['name', 'description', 'author', 'platform', 'language', 'download', 'comments', 'thumbnail', 'is_free', 'rating', 'attachment', 'user_id'];

    protected $casts = [
        'is_free' => 'boolean',
        'download' => 'integer'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function files()
    {
        return $this->hasMany(ContractFile::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function countRatings()
    {
        return $this->ratings()->selectRaw(' ratings.ratingable_id, COUNT(ratings.id) AS count_rating, SUM(ratings.rating) AS sum_rating')
            ->groupBy(['ratingable_id', 'ratingable_type']);
    }


}
