<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractFile extends Model
{
    protected $fillable = [ 'contract_id', 'filename', 'path', 'mime_type', 'extension', 'is_saved' ];

    public $timestamps = false;

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

}
