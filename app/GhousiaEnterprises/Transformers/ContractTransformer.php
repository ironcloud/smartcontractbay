<?php
/**
 * Created by PhpStorm.
 * User: Syed Arif Iqbal
 * Date: 05-Jun-18
 * Time: 3:45 PM
 */

namespace App\GhousiaEnterprises\Transformers;


class ContractTransformer extends Transformer
{

    function transform($contract)
    {
        $rating = array_shift($contract["count_ratings"]);
        return [
            "id" => $contract["id"],
            "name" => $contract["name"],
            "description" => $contract["description"],
            "thumbnail" => public_filepath($contract["thumbnail"]),
            "banner" => public_filepath($contract["attachment"]),
            "author" => $contract["author"],
            "download" => $contract["download"],
            "owner" => [
                "id" => $contract["owner"]['id'],
                "name" => $contract["owner"]['first_name'] . ' ' . $contract["owner"]['last_name'],
                "email" => $contract["owner"]['email'],
            ],
            "platform" => $contract["platform"],
            "files" => $contract["files"],
            "programmingLanguage" => $contract["language"],
            "comments" => $contract["comments"],
            "is_free" => (boolean)$contract["is_free"],
            "price" => (float)$contract["price"],
            "rating" => [
                "count" => $rating? $rating['count_rating']:0,
                "avg_score" => (float)($rating && $rating['count_rating'] > 0) ? $rating['sum_rating']/$rating['count_rating']:0,
                "score" => (float) $rating? $rating['sum_rating']: 0,
            ],
            "created_at" => $contract["created_at"],
        ];
    }
}
