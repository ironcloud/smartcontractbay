<?php
/**
 * Created by PhpStorm.
 * User: Syed Arif Iqbal
 * Date: 05-Jun-18
 * Time: 3:37 PM
 */

namespace App\GhousiaEnterprises\Transformers;


abstract class Transformer
{
    public function transformCollection(array $item)
    {
        return array_map([$this, 'transform'], $item);
    }

    abstract function transform($item);
}