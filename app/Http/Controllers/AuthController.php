<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
//use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'city' => 'required',
            'country' => 'required',
//            'address' => 'required',
        ]);
        $user = new User;
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->password = bcrypt($request->password);
        $user->city = bcrypt($request->city);
        $user->country = bcrypt($request->country);
        $user->gender = bcrypt($request->gender);
        $user->save();
        return response([
            'status' => 'success',
            'data' => $user
        ], 200);
    }
    public function login(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'password'=>'required']);

        $credentials = $request->only('email', 'password');

        $ttl = new \DateTime();
        $ttl->modify("+60 minutes");

        if ( ! $token = JWTAuth::attempt($credentials, ['exp' => $ttl->getTimestamp()])) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'msg' => 'Invalid Credentials.'
            ], 400);
        }

        $user = User::where('email', request('email'))->first();

        $user->dp = public_filepath($user->dp);

        return response([
            'status' => 'success',
            'token' => $token,
            'expirationOn' => $ttl->getTimestamp(),
            'user' => $user
        ]);
    }

    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->dp = !$user->dp?:public_filepath($user->dp);
        return response([
            'status' => 'success',
            'user' => $user
        ]);
    }

    public function logout()
    {
        JWTAuth::invalidate();
        return response([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }

}
