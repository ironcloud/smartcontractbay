<?php

namespace App\Http\Controllers\Api;

use App\Contract;
use App\ContractFile;
use App\GhousiaEnterprises\Transformers\ContractTransformer;
use Illuminate\Http\Request;
use Zip;

class ContractsController extends BaseApiController
{

    protected $contractTransformer;

    /**
     * ContractsController constructor.
     * @param $contractTransformer
     */
    public function __construct(ContractTransformer $contractTransformer)
    {
        $this->contractTransformer = $contractTransformer;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $limit = request('limit', 12);
        $paginater = Contract::with(['owner', 'countRatings', 'files'])->orderBy('id', 'desc')->paginate($limit);
        return $this->respond([
            "data" => $this->contractTransformer->transformCollection($paginater->getCollection()->toArray()),
            "paginater" => [
                "total_count" => $paginater->total(),
                "total_pages" => ceil($paginater->total() / $paginater->perPage() ),
                "limit" => $paginater->perPage(),
                "current_page" => $paginater->currentPage()
            ]
        ]);
    }

    public function myContract(Request $request)
    {
        $limit = request('limit', 12);
        $paginater = $request->user()->contracts()->with(['owner', 'countRatings', 'files'])->paginate($limit);
//        return $paginater;
        return $this->respond([
            "data" => $this->contractTransformer->transformCollection($paginater->getCollection()->toArray()),
            "paginater" => [
                "total_count" => $paginater->total(),
                "total_pages" => ceil($paginater->total() / $paginater->perPage() ),
                "limit" => $paginater->perPage(),
                "current_page" => $paginater->currentPage()
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'fileIds' => 'required|array',
            'author' => 'required',
            'description' => 'required',
            'platform' => 'required',
            'programmingLanguage' => 'required',
            'comments' => 'max:255',
        ]);

        $contract = new Contract($request->all());
        auth()->user()->addContract($contract);

        ContractFile::whereIn('id', $request->input('fileIds'))
            ->update(['contract_id' => $contract->id]);

        return $this->respond($contract);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contract = Contract::find($id);

        if(!$contract)
        {
            return $this->responseNotFound("Contract Not Found!");
        }

        $contract->load('owner');

        $contract->load('files');

        $contract->load('countRatings');
//        dd($contract->toArray());
        return $this->respond([
            "contract" => $this->contractTransformer->transform($contract->toArray())
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Contract $contract
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contract $contract)
    {
        $this->validate($request, [
            'name' => 'required',
            'fileIds' => 'required|array',
            'author' => 'required',
            'description' => 'required',
            'platform' => 'required',
            'programmingLanguage' => 'required',
            'comments' => 'max:255',
        ]);

        $contract->fill($request->all());
        $contract->rating = 0;
        $contract->save();

        ContractFile::whereIn('id', $request->input('fileIds'))
            ->update(['contract_id' => $contract->id]);

        return $this->respond($contract);
    }

    public function rating(Request $request, Contract $contract)
    {
        $this->validate($request, ['rating' => 'required|numeric']);

        $contract->ratingUnique(['rating' => $request->input('rating')], $request->user());

        return $this->respond([
            "data" => [
                "status" => 'success',
                "message" => "Contracted rated success",
                "rating" => $request->input('rating')
            ]
        ]);

    }

    public function download(Request $request, Contract $contract)
    {
        $contract->download += 1;

        $contract->save();

        $files = $contract->files;

        $zip = Zip::create($contract->name . '.zip');

        foreach($files as $file){
            $zip->add(storage_path('app/'.$file->path));
        };

        $zip->close();

//        unlink(public_path($contract->name.'.zip'));

        $headers = array(
            'Content-Type: application/octet-stream',
        );

        return response()->download(public_path().'\\'.$contract->name.'.zip', $contract->name, $headers);

//        return $this->respond([
//            "data" => [
//                "status" => 'success',
//                "message" => "Download success",
//                "count" => $contract->download,
//                "file" => url($contract->name.'.zip'),
//            ]
//        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
