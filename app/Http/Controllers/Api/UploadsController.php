<?php

namespace App\Http\Controllers\Api;

use App\Contract;
use App\ContractFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadsController extends BaseApiController
{
    public function contractUploads(Request $request, $contract_id = false)
    {
        $this->validate($request, ['file' => 'file|mimetypes:text/plain']);

        $path = $request->file->store('public/contracts');

        $fileModel = new ContractFile();

        if ($contract_id)
        {
            $contract = Contract::find($contract_id);
            $fileModel->contract_id = $contract->id;
        }

        $fileModel->filename = $request->file->getClientOriginalName();
        $fileModel->path = $path;
        $fileModel->extension = $request->file->extension();
        $fileModel->mime_type = $request->file->getMimeType();
        $fileModel->save();

        return $this->respond([
            "data" => $fileModel
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contractThumbnail(Request $request)
    {
        $path = $request->thumbnail->store('public/contracts');
        return public_filepath($path);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroyFile(Request $request)
    {
        $this->validate($request, ['id' => 'required|numeric']);

        $file = ContractFile::find($request->input('id'));

        if (unlink(storage_path('app/'.$file->path)))
        {
            $file->delete();
            return $this->respond(['data' => ['success' => true, 'message' => 'File Deleted!']]);
        }

        return $this->responseNotFound('File not found!');

    }
}
