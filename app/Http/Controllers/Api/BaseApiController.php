<?php
/**
 * Created by PhpStorm.
 * User: Syed Arif Iqbal
 * Date: 05-Jun-18
 * Time: 5:59 PM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class BaseApiController extends Controller
{

    protected $statusCode = 200;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     * @return BaseApiController
     */
    public function setStatusCode(int $statusCode): BaseApiController
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function responseNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->responseWithError($message);
    }

    public function responseWithError($message = 'Some Error!')
    {
        return $this->respond([
            "error" => [
                "message" => $message,
                'statusCode' => $this->getStatusCode()
            ]
        ]);
    }

    public function respond($data, $header = [])
    {
        return Response::json($data, $this->getStatusCode(), $header);
    }

}