import authService from "../services/authService";
import Vue from 'vue';
import Vuex from 'vuex';

// console.log(Vue);

Vue.use(Vuex);

const state = {
    isAuthenticated: false,
    user: null
};

export const store = new Vuex.Store({

    state,

    getters: {

        isAuthenticated: (state) => {
            return state.isAuthenticated
        },

        user: (state) => {
            if (state.user)
                return state.user;

            authService.profile().then((user) => {
                state.user = user;
                return state.user
            });
        }
    },

    actions:{
        logout(context){
            context.commit('logout');
        },

        login (context, credentials) {
            return new Promise(resolve => {
                authService.login(credentials)
                    .then(response => {
                        context.commit('login', response);
                        resolve(response);
                    })
            }, rejects => { console.log(rejects.response) })
        }
    },

    mutations: {
        logout(state)
        {
            if (typeof window !== 'undefined') {
                window.localStorage.removeItem('token');
                window.localStorage.removeItem('expirationOn');
            }
            state.isAuthenticated = false;
        },

        login(state, data)
        {
            if (typeof window !== 'undefined') {
                window.localStorage.setItem('token', data.token);
                window.localStorage.setItem('expirationOn', data.expirationOn);
                window.localStorage.setItem('user', JSON.stringify(data.user));
            }
            state.isAuthenticated = true;
            state.user = data.user;
        },

        updateUser: function (state, user) {
            Object.assign(state, user);
        }
    }
});

if (typeof window !== 'undefined') {
    document.addEventListener('DOMContentLoaded', e => {
        let expirationOn = window.localStorage.getItem('expirationOn');
        let unixTimestamps = new Date().getTime() / 1000;
        if (expirationOn != null && parseInt(expirationOn) - unixTimestamps > 0) {
            store.state.isAuthenticated = true;
        }
    })
}

export default store;