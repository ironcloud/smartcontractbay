import VueRouter from 'vue-router';

Vue.use(VueRouter);

let routes = [
    {
        path: '/',
        component: require('./pages/Home')
    },
    {
        path: '/profile',
        component: require('./pages/Profile')
    },
    {
        path: '/contracts',
        component: require('./pages/contracts')
    },
    {
        path: '/contracts/:id',
        component: require('./pages/ContractShow')
    },
    {
        path: '/contracts/:id/edit',
        component: require('./pages/UploadContract')
    },
    {
        path: '/upload-contract',
        component: require('./pages/UploadContract')
    },
    {
        path: '/login',
        name: 'login',
        component: require('./pages/Login')
    },
    {
        path: '/signup',
        component: require('./pages/Signup')
    },
    {
        path: '/logout',
        component: require('./pages/Logout')
    }

];

export default new VueRouter({
    scrollBehavior: (to, from, savedPosition) => ({ y: 0 }),
    routes,
    linkActiveClass: 'is-active',
    mode: 'history'
});