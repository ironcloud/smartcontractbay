
const authService = {
    login (credentials) {
        return new Promise((resolve) => {
            axios.post('/login', credentials)
                .then(response => resolve(response.data))
                .catch(error => console.log(error.response))
        })
    },

    profile()
    {
        return new Promise((resolve) => {
            axios.get('auth/user?token='+window.localStorage.getItem('token'))
                .then(res => {
                    // console.log(res, 'from services')
                    resolve(res.data.user);
                });
        });
    }
};

export default authService
