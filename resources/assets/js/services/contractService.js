const contractService = {

    download(contract) {
        return new Promise((resolve) => {
            axios.post(`contracts/${contract.id}/download`, {}, { responseType: 'arraybuffer' })
                .then( (data) => {
                    // window.open(data.data.file, "_blank");
                    // console.log(data);
                    let headers = data.headers;
                    let blob = new Blob([data.data], { type: headers['content-type'] });
                    let link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = `${contract.name}.zip`;
                    link.click();
                    resolve(data);
                }).catch(e => { console.log(e.response) });
        });
    },

    getById(contractId)
    {
        return new Promise((resolve) => {
            axios.get(`contracts/${contractId}`)
                .then(res => {
                    resolve(res.data.contract);
                })
                .catch(error => console.log(error.response))
        });
    },

    get(pageNo = 1)
    {
        return new Promise((resolve) => {
            axios.get(`contracts?page=${pageNo}`)
                .then(response => resolve(response) )
                .catch(error => console.log(error.response));

            // axios.get(`contracts/${contractId}`)
            //     .then(res => {
            //         resolve(res.data.contract);
            //     })
        });
    }
};

export default contractService
