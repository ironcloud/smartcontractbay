export function isAuthenticated()
{
    return window.localStorage.getItem('token');
}

export function getUser(then)
{
    return axios.get('api/auth/user')
        .then(res => then(res));
}