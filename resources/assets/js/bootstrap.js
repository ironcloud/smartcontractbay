import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import VueTruncate from 'vue-truncate-filter';
import store from "./store/store";

window.Vue = Vue;
window.Vuex = Vuex;

Vue.use(VueTruncate);
Vue.use(Vuex);
// Vue.use(Simplert);

window.Event = new Vue();

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.baseURL = 'http://localhost:8000/api';

window.axios.interceptors.response.use(null, function(error) {
    if (error.response.status === 401) {
        window.localStorage.removeItem('token');
    }
    return Promise.reject(error);
});

if (typeof window !== 'undefined') {
    document.addEventListener('DOMContentLoaded', () =>
    {
        let AUTH_TOKEN = window.localStorage.getItem('token');
        let expirationOn = window.localStorage.getItem('expirationOn');
        let unixTimestamps = new Date().getTime() / 1000;
        if ((expirationOn != null && parseInt(expirationOn) - unixTimestamps > 0) && AUTH_TOKEN )
        {
            window.axios.defaults.headers.common['Authorization'] = "Bearer "+AUTH_TOKEN;
            axios.defaults.headers.common['Authorization'] = "Bearer "+AUTH_TOKEN;
        }
    })
}

Vue.prototype.$http = axios;

// let token = document.head.querySelector('meta[name="csrf-token"]');
//
// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }
