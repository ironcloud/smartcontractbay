import './bootstrap';
import router from './routes';
import foot from './components/FooterComponent';
import { store } from './store/store';
import { mapGetters } from 'vuex';

const app = new Vue({

    store: store,

    components: {
        foot
    },

    el: '#app',

    data: {
        showNav: false,
    },

    computed:{
        ...mapGetters(['isAuthenticated', 'user'])
    },

    created(){
        console.log(this.isAuthenticated);
        // Event.$on('loggedin', u => this.user = u );
        // Event.$on('loggedout', () => this.user = null );
        // if(isAuthenticated())
        // {
        //     this.$http.get('api/auth/user')
        //         .then(res => {
        //             this.user = res.data.user;
        //             window.localStorage.setItem('user', JSON.stringify(res.data.user));
        //             this.$store.commit('updateUser', res.data.user);
        //             this.$store.state.user = res.data.user;
        //         });
        // }
    },

    router

});
