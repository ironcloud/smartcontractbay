 <nav class="navbar is-primary">
    <div class="container">
        <div class="navbar-brand">

            <router-link to="/" class="navbar-item logo" exact>
                <h3 class="title is-3">
                    <img src="/images/logo.png" alt="Logo">
                </h3>
            </router-link>

            <span class="navbar-burger burger" :class="{'is-active': showNav}" data-target="navbarMenuHeroA" @click.prevent="showNav = !showNav">
                <span></span>
                <span></span>
                <span></span>
            </span>

        </div>
        <div id="navbarMenuHeroA" class="navbar-menu" :class="{'is-active': showNav}">

            <div class="navbar-end">

                <router-link to="/contracts" class="navbar-item">
                    Contracts
                </router-link>

                <router-link to="/login" class="navbar-item" v-if="!isAuthenticated">Login</router-link>

                <router-link to="/upload-contract" class="navbar-item" v-if="isAuthenticated">
                    <span class="icon"><i class="fa fa-upload"></i></span> Upload Contract
                </router-link>

                <div class="navbar-item has-dropdown is-hoverable" v-if="isAuthenticated">
                    <a class="navbar-link" href="#">

                        <figure class="image is-48x48 dp-image">
                            <img :src="user && user.dp" >
                        </figure>
                        <span v-if="user">
                            @{{  user.first_name }}  @{{  user.last_name }}
                        </span>
                    </a>
                    <div class="navbar-dropdown">

                        <router-link to="/profile" class="navbar-item">Profile</router-link>

                        <hr class="navbar-divider">
                        <router-link to="/logout" class="navbar-item" @click="logout()">Logout</router-link>

                    </div>

                </div>

            </div>

        </div>

    </div>

 </nav>